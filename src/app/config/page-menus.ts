"use strict";
var pageMenus = [{
  'icon': 'fa fa-th-large',
  'title': 'Dashboard',
  'url': '',
  'caret': 'true',
  'submenu': [{
    'url': 'dashboard/v1',
    'title': 'Dashboard v1'
  }, {
    'url': 'dashboard/v2',
    'title': 'Dashboard v2'
  }]
}];

export default pageMenus;
