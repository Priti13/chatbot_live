"use strict";
var themeConf_ = {
  "defaultTheme": "apple",
  "defaultColor": "default",
  "selectedTheme": "apple",
  "selectedColor": "default",
  "selectThemeColorList": [
    [
      {
        "Title": "Red",
        "css": "bg-red",
        "class": "red"
      },
      {
        "Title": "Pink",
        "css": "bg-pink",
        "class": "pink"
      },
      {
        "Title": "Orange",
        "css": "bg-orange",
        "class": "orange"
      },
      {
        "Title": "Yellow",
        "css": "bg-yellow",
        "class": "yellow"
      },
      {
        "Title": "Lime",
        "css": "bg-lime",
        "class": "lime"
      },
      {
        "css": "bg-green",
        "Title": "Green",
        "class": "green"
      }
    ],
    [
      {
        "Title": "Teal",
        "css": "bg-teal",
        "class": "teal"
      },
      {
        "Title": "Aqua",
        "css": "bg-aqua",
        "class": "aqua"
      },
      {
        "Title": "Black",
        "css": "bg-black",
        "class": "black"
      },
      {
        "Title": "blue",
        "css": "bg-blue",
        "class": "blue"
      },
      {
        "Title": "purple",
        "css": "bg-purple",
        "class": "purple"
      },
      {
        "css": "bg-indigo",
        "Title": "Indigo",
        "class": "indigo"
      }
    ]
  ],
  "availableThemes": [
    {
      "themeName": "Default",
      "icon": "assets/img/theme/default.jpg",
      "css": "default"
    },
    {
      "themeName": "Material",
      "icon": "assets/img/theme/material.jpg",
      "css": "material"
    },
    {
      "themeName": "Apple",
      "icon": "assets/img/theme/apple.jpg",
      "css": "apple"
    }
  ],
  "getAmChartsTheme": function () {
    return 'none';
  }
};

export default themeConf_;