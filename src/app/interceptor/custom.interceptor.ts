import {Injectable} from "@angular/core"
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders} from "@angular/common/http"
import {Observable} from 'rxjs'


@Injectable()

export class CustomInterceptor implements HttpInterceptor
{
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        const token = localStorage.getItem('token');
        if(req.url.indexOf("login")!=-1){
            let newRequest = req.clone({
                headers : new HttpHeaders({"Content-Type": "application/x-www-form-urlencoded" })
            })
            // debugger
            return next.handle(newRequest);
        } 
        else{ 
        let newRequest = req.clone({
            headers : new HttpHeaders({"Content-Type": "application/json",  'Authorization': `Bearer ${token}` })
        })
        // debugger
        return next.handle(newRequest);
        }
    }

} 