import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LeftSidebarComponent } from './left-sidebar/left-sidebar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    FormsModule,
    HttpModule,
    CommonModule,
    RouterModule,
    NgbModule.forRoot()
  ],
  declarations: [HeaderComponent, FooterComponent, LeftSidebarComponent],
  exports: [HeaderComponent, FooterComponent, LeftSidebarComponent]
})
export class LayoutModule { }
