import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewChatBotComponent } from './new-chat-bot.component';

describe('NewChatBotComponent', () => {
  let component: NewChatBotComponent;
  let fixture: ComponentFixture<NewChatBotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewChatBotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewChatBotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
