import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TimeFilterService } from './time-filter/time-filter.service.component';
import { NgModule } from '@angular/core';
import { TimeFilterComponent } from "./time-filter/time-filter.component";
import { Daterangepicker } from 'ng2-daterangepicker';
// import { ClickOutSideDirectives } from '../directives_/clickOutSide.directive';
import { SharedServices } from './shared.services';

@NgModule({
  imports: [CommonModule, FormsModule, Daterangepicker], 
  declarations: [TimeFilterComponent], 
  exports: [TimeFilterComponent], 
  providers: [TimeFilterService,SharedServices
  ]
})
export class SharedModule { }
