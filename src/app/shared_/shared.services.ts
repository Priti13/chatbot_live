import { Injectable } from '@angular/core';
@Injectable()
export class SharedServices {

    public sortTabReportsByColSpan(tabReports) {
        var twelve = Object.assign([], tabReports);
        var six = [];
        var four = [];
        var eight = [];
        var sorted_reports = [];
        this.separateAllBySolSpan(twelve, four, six, eight);
        sorted_reports = this.mergeAll(6, sorted_reports, twelve, six, eight, four);
        sorted_reports = this.mergeAll(12, sorted_reports, twelve, six, eight, four);
        sorted_reports = this.mergeAll(8, sorted_reports, twelve, six, eight, four);
        sorted_reports = this.mergeAll(4, sorted_reports, twelve, six, eight, four);
        sorted_reports = this.mergeAll('all', sorted_reports, twelve, six, eight, four);
        return sorted_reports;
    }

    /**
     * 
     * @param type merge by alias
     * @param sorted_reports // target array
     * @param twelve  // array of  elements having colSpan 12
     * @param six   // array of  elements having colSpan 6
     * @param eight // array of  elements having colSpan 8
     * @param four // array of  elements having colSpan 4
     */
    private mergeAll(type, sorted_reports, twelve, six, eight, four) {
        switch (type) {
            case 12:
                sorted_reports = sorted_reports.concat(twelve);
                twelve.length = 0;
                break;
            case 6:
                let arr = (six.length % 2 == 0) ? six.splice(0, six.length) : six.splice(0, six.length - 1)
                sorted_reports = sorted_reports.concat(arr);
                break;
            case 8:
                for (var i = 0; i < eight.length; i++) {
                    if (four.length > 0) {
                        sorted_reports.push(eight.shift());
                        sorted_reports.push(four.shift());
                        i = i - 1;
                    } else {
                        break;
                    }
                }
                break;
            case 4:
                let fourarr = (four.length % 3 == 0) ? four.splice(0, four.length) : four.splice(0, (four.length - four.length % 3))
                sorted_reports = sorted_reports.concat(fourarr);
                break;
            case 'all':
                sorted_reports = sorted_reports.concat(six, four, eight);
        }
        return sorted_reports;
    }

    /**
     * This f/n will separte main array by colSpan to their respectives Arrays.
     * @param twelve array contains all 12's
     * @param four  array contains all 4's
     * @param six array contains all 6's
     * @param eight array contains all 8's
     */
    private separateAllBySolSpan(twelve, four, six, eight) {
        for (var i = 0; i < twelve.length; i++) {
            switch (twelve[i].colSpan) {
                case 4:
                    four.push(twelve[i]);
                    twelve.splice(i, 1);
                    i = i - 1;
                    break;
                case 6:
                    six.push(twelve[i]);
                    twelve.splice(i, 1);
                    i = i - 1;
                    break;
                case 8:
                    eight.push(twelve[i]);
                    twelve.splice(i, 1);
                    i = i - 1;
                    break;
            }
        }
    }

    public sortByKey(array, key) {
        return array.sort(function (a, b) {
            var x = a[key]; var y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    }

}
