import { Subscription } from 'rxjs/Subscription';
import { Subscriber } from 'rxjs/Subscriber';
import { TimeFilterService } from './time-filter.service.component';
import { Component, OnInit, NgModule, Input } from '@angular/core';
import { Daterangepicker } from 'ng2-daterangepicker';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { config } from 'rxjs';

declare var moment: any;


@Component({
  selector: 'app-time-filter',
  templateUrl: './time-filter.component.html',
  styleUrls: ['./time-filter.component.css'],
})


export class TimeFilterComponent implements OnInit {

  public dateFrom: any;
  public dateTo: any;
  public timeZone: any;
  @Input() configStyle: any;

  public dateFormat = "DD-MM-YYYY h:mm:ss";

  public tsSubscribers: Subscription;

  public timeInfo: any = {
    date: {
      start: moment().subtract(1, 'month').format(this.dateFormat),
      end: moment().format(this.dateFormat)
    },
    timestamp: {
      start: Date.parse(moment().subtract(1, 'month')),
      end: Date.parse(moment()),
    }
  };






  ngOnInit() {
    this.daterangepickerOptions.settings = this.buildConfigurations();

    this.tsSubscribers = this.timeFilterService.getTimeFilterSubscriber().subscribe(timeobj => {
      this.timeInfo = timeobj;
    });

  }


  public dateInputs: any = [
    {
      start: moment().subtract(12, 'month'),
      end: moment().subtract(6, 'month')
    },
    {
      start: moment().subtract(9, 'month'),
      end: moment().subtract(6, 'month')
    },
    {
      start: moment().subtract(4, 'month'),
      end: moment()
    },
    {
      start: moment(),
      end: moment().add(5, 'month'),
    }
  ];

  /**
   *  will set date range by default
   */
  public mainInput = {
    start: moment().subtract(1, 'month'),
    end: moment()
  }

  public singlePicker = {
    singleDatePicker: false,
    showDropdowns: false,
    opens: "left"
  }

  public singleDate: any;
  public eventLog = '';


  public buildConfigurations() {
    return {
      locale: { format: this.dateFormat },
      alwaysShowCalendars: false,
      opens: this.configStyle ? this.configStyle.openPos ? this.configStyle.openPos : 'right' : 'right',
      timePicker: true,
      ranges: this.getCustomRanges()
    }
  };

  public getCustomRanges() {
    return {
      'This Month': [moment(moment().startOf('month')).startOf('day'), moment()],
      'Last Month': [
        moment(moment(moment().startOf('month')).subtract(1, 'month').startOf('month')).startOf('day'),
        moment(moment().startOf('month')).subtract(1, 'month').endOf('month')
      ],
      'Last 3 Months': [
        moment(moment(moment().startOf('month')).subtract(3, 'month').startOf('month')).startOf('day'),
        moment(moment().startOf('month')).subtract(1, 'month').endOf('month')
      ],
      'Last 6 Months': [
        moment(moment(moment().startOf('month')).subtract(6, 'month').startOf('month')).startOf('day'),
        moment(moment().startOf('month')).subtract(1, 'month').endOf('month')
      ],
      'Last 12 Months': [
        moment(moment(moment().startOf('month')).subtract(12, 'month').startOf('month')).startOf('day'),
        moment(moment().startOf('month')).subtract(1, 'month').endOf('month')
      ],
    };
  }


  constructor(private daterangepickerOptions: DaterangepickerConfig, private timeFilterService: TimeFilterService) {
    this.singleDate = this.timeInfo.timestamp.end;
  }

  public selectedDate(value: any, dateInput: any) {
    console.log("Date selected  : " + JSON.stringify(dateInput) + "value : " + JSON.stringify(value));
    console.log("start : " + this.mainInput.start + " end: " + this.mainInput.end);
    dateInput.start = value.start;
    dateInput.end = value.end;

    let startDateInTimeStamp = Date.parse(value.start);
    console.log("startDateInTimeStamp", startDateInTimeStamp);
    // this.timeFilterService.changeStartDate("" + startDateInTimeStamp); // bind startDate to timeservice  
    let endDateInTimeStamp = Date.parse(value.end);
    console.log("startDateInTimeStamp", endDateInTimeStamp);
    //this.timeFilterService.changeEndDate("" + endDateInTimeStamp); // bind endDate to timeservice 

    this.setDatetoTimeServices(value.start, value.end, startDateInTimeStamp, endDateInTimeStamp);
  }

  public singleSelect(value: any) {
    this.singleDate = value.start;
  }

  public applyDate(value: any, dateInput: any) {
    dateInput.start = value.start;
    dateInput.end = value.end;
  }

  public calendarEventsHandler(e: any) {
    this.eventLog += '\nEvent Fired: ' + e.event.type;
  }


  public setDatetoTimeServices(dateStart, dateEnd, tsStart, tsEnd) {
    console.log("Filter :dS " + dateStart.format("DD-MM-YYYY h:mm:ss") + " dE: " + dateEnd + " tsS: " + tsStart + " tsE: " + tsEnd);
    this.timeFilterService.setTimeInfo(dateStart.format(this.dateFormat), dateEnd.format(this.dateFormat), tsStart, tsEnd);
  }




}
