// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url: 'http://13.68.186.88:5504'
};

/*
  // public url = ''https://gcscdev.in2ittech.com:443';
  // public url = 'http://150.107.240.144:5504';
  // public url = 'http://13.68.186.88:5504'; https://www.gcscdev.in2ittech.com
  //  public url = 'https://gcscdev.in2ittech.com:5505';

 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
